﻿using DocuMerge.IDC;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Services.Client;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace DocuMerge
{
    class Program
    {
        private static Application _app = new Application();
        private static object _missing = System.Reflection.Missing.Value;
        private static readonly string TempPath = ConfigurationManager.AppSettings["temp"].ToString();
        private static readonly string Rest = ConfigurationManager.AppSettings["REST"].ToString();
        private static readonly string Siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        //eventually put these in the config
        protected static string ListInterviewDocs = "Interview Documents";        

        public static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\DocuMerge{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                Console.WriteLine("DocuMerge started");
                Logger.Info("DocuMerge started");
                ProcessInterviewPanelRequests(); 
                ProcessCandidateRequests();
            }
            catch (Exception ex)
            {
                object saveChanges2 = false;
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
                try
                {
                    _app.Quit(saveChanges2);
                    _app = null;
                }
                catch(Exception e)
                {
                    Logger.Error(e.Message);
                }

                GC.Collect();
            }
            finally
            {

                try
                {
                    object saveChanges = false;
                    _app?.Quit(saveChanges);
                    _app = null;
                    GC.Collect();
                }
                catch(Exception e)
                {
                    Logger.Error(e.Message);
                }
                Environment.Exit(0);
            }
            
        }

        private static void ProcessInterviewPanelRequests()
        {
            try
            {
                var interviewFields = new List<string>();
                var ipContext = new InterviewGuideToolDataContext(new Uri(Rest))
                {
                    Credentials = System.Net.CredentialCache.DefaultCredentials
                };
                var itemsCreated = false;                
                Console.WriteLine("Checking for new Role Specific Interview Guide requests");
                Logger.Info("Checking for new Role Specific Interview Guide requests");
                var ipItems = ipContext.InterviewPanel.Where(x => x.DocsCompleted == false).ToList();
                foreach (var l in ipItems)
                {
                    if (!l.IsCreated == true)
                    {
                        try
                        {
                            if (_app == null)
                            {
                                _app = new Application();
                            }
                            interviewFields.Add("ManagerCompetency, HiringManager, ManagerPanel, PeerPanel, DirectReports, OtherPanel");
                            interviewFields.Add("PeerCompetency, HiringManager, ManagerPanel, PeerPanel, DirectReports, OtherPanel");
                            interviewFields.Add("DirectReportCompetency, HiringManager, ManagerPanel, PeerPanel, DirectReports, OtherPanel");
                            interviewFields.Add("OtherCompetency, HiringManager, ManagerPanel, PeerPanel, DirectReports, OtherPanel");

                            foreach (var field in interviewFields)
                            {
                                try
                                {
                                    var idContext = new InterviewGuideToolDataContext(new Uri(Rest))
                                    {
                                        Credentials = CredentialCache.DefaultCredentials
                                    };
                                    var ipFieldItem = idContext.InterviewPanel.Expand(field).Where(x => x.Id.Equals(l.Id)).FirstOrDefault();
                                    object brak = WdBreakType.wdPageBreak;
                                    ProcessInterviewDocuments(idContext, ipFieldItem, ref brak);
                                    var lastItem = interviewFields.Last();
                                    if (field.Equals(lastItem))
                                    {
                                        ipFieldItem.IsCreated = true;
                                        ipFieldItem.DocsCompleted = true;
                                        idContext.UpdateObject(ipFieldItem);
                                        idContext.SaveChanges();
                                        itemsCreated = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    Logger.Error(ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Logger.Error(ex.Message);
                        }
                        finally
                        {
                            object saveChanges = false;
                            _app?.Quit(saveChanges);
                            _app = null;
                        }
                    }
                }
                if (itemsCreated)
                {
                    Console.Write("All Role Specific Interview Guide work is complete!\r\n\r\n");
                    Logger.Info("All Role Specific Interview Guide work is complete!\r\n\r\n");
                }
                else
                {
                    Console.WriteLine("No new Role Specific Interview Guide requests");
                    Logger.Info("No new Role Specific Interview Guide requests");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
                GC.Collect();
            }
            finally
            {
                try
                {
                    GC.Collect();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.Error(ex.Message);
                }
            }
        }

        private static void ProcessCandidateRequests()
        {
            try
            {
                var candidateFields = new List<string>();
                var cipContext = new InterviewGuideToolDataContext(new Uri(Rest))
                {
                    Credentials = CredentialCache.DefaultCredentials
                };
                var cipItems = new List<CandidateInterviewItem>();
                var itemsCreated = false;                
                Console.WriteLine("Checking for new Candidate Specific Supplement requests");
                Logger.Info("Checking for new Candidate Specific Supplement requests");
                cipItems = cipContext.CandidateInterview.Where(x => x.DocsCompleted == false).ToList();
                foreach (var l in cipItems)
                {
                    Console.WriteLine("Item IsCreated: " + l.IsCreated);
                    if (!l.IsCreated == true)
                    {
                        try
                        {
                            if (_app == null)
                            {
                                _app = new Application();
                            }

                            candidateFields.Add("ManagerCompetency");
                            candidateFields.Add("PeerCompetency");
                            candidateFields.Add("DirectReportCompetency");
                            candidateFields.Add("OtherCompetency");

                            foreach (var field in candidateFields)
                            {
                                Console.WriteLine("Field " + field);
                                try
                                {
                                    var cidContext = new InterviewGuideToolDataContext(new Uri(Rest));
                                    cidContext.Credentials = System.Net.CredentialCache.DefaultCredentials;
                                    var cipFieldItems = new CandidateInterviewItem();
                                    cipFieldItems = cidContext.CandidateInterview.Expand(field).Where(x => x.Id.Equals(l.Id)).FirstOrDefault();
                                    Console.WriteLine("cipFieldItems Path: " + cipFieldItems.Path);
                                    object brak = WdBreakType.wdPageBreak;
                                    ProcessCandidateDocuments(cidContext, cipFieldItems, ref brak);
                                    var lastItem = candidateFields.Last();
                                    if (field.Equals(lastItem))
                                    {
                                        cipFieldItems.IsCreated = true;
                                        cipFieldItems.DocsCompleted = true;
                                        cidContext.UpdateObject(cipFieldItems);
                                        cidContext.SaveChanges();
                                        itemsCreated = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    Logger.Error(ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Logger.Error(ex.Message);
                        }
                        finally
                        {
                            object saveChanges = false;
                            _app?.Quit(saveChanges);
                            _app = null;
                        }

                    }
                }
                if (itemsCreated)
                {
                    Console.Write("All Candidate Specific Supplement work is complete!\r\n\r\n");
                    Logger.Info("All Candidate Specific Supplement work is complete!\r\n\r\n");
                }
                else
                {
                    Console.WriteLine("No new Candidate Specific Supplement requests");
                    Logger.Info("No new Candidate Specific Supplement requests");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
                GC.Collect();
            }
            finally
            {
                try
                {
                    GC.Collect();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.Error(ex.Message);
                }
            }
        }

        private static void ProcessInterviewDocuments(InterviewGuideToolDataContext idContext, InterviewPanelItem p, ref object brak)
        {
            var serverURL = "";

            try
            {
                IOrderedEnumerable<IGrouping<string, CompetencyQuestionsItem>> questions = null;
                if (p.ManagerCompetency.Count > 0)
                {
                    questions = from x in p.ManagerCompetency
                                group x by x.Stakeholder into newGroup
                                orderby newGroup.Key
                                select newGroup;
                }

                if (p.PeerCompetency.Count > 0)
                {
                    questions = from x in p.PeerCompetency
                                group x by x.Stakeholder into newGroup
                                orderby newGroup.Key
                                select newGroup;
                }

                if (p.DirectReportCompetency.Count > 0)
                {
                    questions = from x in p.DirectReportCompetency
                                group x by x.Stakeholder into newGroup
                                orderby newGroup.Key
                                select newGroup;
                }

                if (p.OtherCompetency.Count > 0)
                {
                    questions = from x in p.OtherCompetency
                                group x by x.Stakeholder into newGroup
                                orderby newGroup.Key
                                select newGroup;
                }

                if (questions == null) return;
                foreach (var nameGroup in questions)
                {
                    Console.WriteLine("Key: {0}", nameGroup.Key);
                    Logger.Info("Key: {0}", nameGroup.Key);

                    var rand = new Random();
                    var randomId = rand.Next();

                    if (!Directory.Exists(TempPath))
                    {
                        Directory.CreateDirectory(TempPath);
                    }
                    else
                    {
                        var files = Directory.GetFiles(TempPath);

                        foreach (var s in files)
                        {
                            try
                            {
                                File.Delete(s);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                Logger.Error(ex.Message);
                            }
                        }
                    }

                    //get top document
                    using (var client = new WebClient())
                    {
                        client.Credentials = CredentialCache.DefaultCredentials;
                        client.DownloadFile(new Uri(Siteurl + @"/Shared%20Documents/Interview%20Guide%20Top.docx"), TempPath + "\\" + "Interview Guide Top.docx");
                    }

                    var doc = _app.Documents.Open(TempPath + "\\" + "Interview Guide Top.docx");
                    Console.WriteLine("Hiring Manager " + p.HiringManager.Name);
                    Logger.Info("Hiring Manager " + p.HiringManager.Name);
                    Console.WriteLine("Job Title " + p.JobTitle);
                    Logger.Info("Job Title " + p.JobTitle);
                    //top document bookmarks
                    InsertBookMark(doc, doc.Bookmarks["HiringManager"], p.HiringManager.Name);                        
                    InsertBookMark(doc, doc.Bookmarks["JobTitle"], p.JobTitle + " - " + p.JobNumber);                        

                    object objUnit = WdUnits.wdStory;
                    _app.Selection.EndKey(ref objUnit, _missing);
                    _app.Selection.InsertBreak(ref brak);

                    var selectedCompetencies = new List<string>();

                    foreach (var stake in nameGroup)
                    {
                        ProcessQuestions(randomId, doc, stake);

                        //Write competencies 
                        if (stake.Manager != null && stake.Manager.Any())
                        {
                            selectedCompetencies.Add(stake.Manager);
                        }
                        if (stake.Peer != null &&stake.Peer.Any())
                        {
                            selectedCompetencies.Add(stake.Peer);
                        }
                        if (stake.DirectReport!= null && stake.DirectReport.Any())
                        {
                            selectedCompetencies.Add(stake.DirectReport);
                        }
                        if (stake.Other != null && stake.Other.Any())
                        {
                            selectedCompetencies.Add(stake.Other);
                        }
                    }

                    //get bottom document
                    using (var client = new WebClient())
                    {
                        client.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        client.DownloadFile(new System.Uri(Siteurl + "/Shared%20Documents/Interview%20Guide%20Bottom.docx"), TempPath + "\\" + "Interview Guide Bottom.docx");
                        AddDocument(TempPath + "Interview Guide Bottom.docx", doc, true);
                    }

                    //bottom document Bookmarks
                    var count = 1;                        
                    foreach (var selectedCompetency in selectedCompetencies)
                    {
                        var competency = "Competency_" + count.ToString();
                        InsertBookMark(doc, doc.Bookmarks[competency], selectedCompetency);
                        count++;
                    }
                    InsertBookMark(doc, doc.Bookmarks["HiringManagerBottom"], p.HiringManager.Name);
                    InsertBookMark(doc, doc.Bookmarks["JobTitleBottom"], p.JobTitle + " - " + p.JobNumber);

                    if (!Directory.Exists(TempPath + "output"))
                    {
                        Directory.CreateDirectory(TempPath + "output");
                    }

                    const string pattern = "[\\~#%&*{}/:<>?|\"-]";
                    const string replacement = "_";

                    var regEx = new Regex(pattern);
                    var jobnumber = Regex.Replace(regEx.Replace(p.JobNumber, replacement), @"\s+", " ");
                    var jobtitle = Regex.Replace(regEx.Replace(p.JobTitle, replacement), @"\s+", " ");

                    var savepath = TempPath + jobnumber + "_" + jobtitle + "_" + nameGroup.Key + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".docx";
                    savepath = savepath.Replace('/', '-');
                    Console.WriteLine(savepath);
                    Logger.Info(savepath);

                    doc.SaveAs2(savepath);
                    doc.Close();

                    serverURL = UploadDoc(savepath, p, idContext);

                    Console.WriteLine("Work is complete for key: {0}", nameGroup.Key);
                    Logger.Info("Work is complete for key: {0}", nameGroup.Key);
                }
            }
            catch (Exception ex)
            {
                object saveChanges2 = false;
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
                _app?.Quit(saveChanges2);
                _app = null;
                GC.Collect();
            }
        }

        private static void ProcessCandidateDocuments(InterviewGuideToolDataContext idContext, CandidateInterviewItem p, ref object brak)
        {
            var serverUrl = "";
           
            try
            {
                if (!p.IsCreated != true) return;
                IOrderedEnumerable<IGrouping<string, CandidateCompetencyQuestionsItem>> questions = null;
                if (p.ManagerCompetency.Count > 0)
                {
                    questions = from x in p.ManagerCompetency
                        group x by x.StakeholderGroup into newGroup
                        orderby newGroup.Key
                        select newGroup;
                }

                if (p.DirectReportCompetency.Count > 0)
                {
                    questions = from x in p.DirectReportCompetency
                        group x by x.StakeholderGroup into newGroup
                        orderby newGroup.Key
                        select newGroup;
                }

                if (p.PeerCompetency.Count > 0)
                {
                    questions = from x in p.PeerCompetency
                        group x by x.StakeholderGroup into newGroup
                        orderby newGroup.Key
                        select newGroup;
                }

                if (p.OtherCompetency.Count > 0)
                {
                    questions = from x in p.OtherCompetency
                        group x by x.StakeholderGroup into newGroup
                        orderby newGroup.Key
                        select newGroup;
                }

                if (questions == null) return;
                foreach (var nameGroup in questions)
                {
                    Console.WriteLine("nameGroup " + nameGroup);
                    Console.WriteLine("Key: {0}", nameGroup.Key);
                    Logger.Info("nameGroup " + nameGroup);
                    
                    Logger.Info("Key: {0}", nameGroup.Key);

                    var rand = new Random();
                    var randomId = rand.Next();
                    Console.WriteLine("RandomID: " + randomId);
                    Console.WriteLine("TempPath: " + TempPath);
                    Logger.Info("RandomID: " + randomId);
                    Logger.Info("TempPath: " + TempPath);
                    if (!Directory.Exists(TempPath))
                    {
                        Logger.Info("Directory Doesn't exist");
                        Directory.CreateDirectory(TempPath);
                        Logger.Info("Created Directory");
                    }
                    else
                    {
                        var files = Directory.GetFiles(TempPath);
                        Logger.Info("Files Length: " + files.Length);
                        foreach (var s in files)
                        {
                            Logger.Info("File: " + s);
                            try
                            {
                                File.Delete(s);
                                Logger.Info("File " + s + " is deleted");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                Logger.Error(ex.Message);
                            }
                        }
                    }

                    //get top document
                    using (var client = new WebClient())
                    {
                        client.Credentials = CredentialCache.DefaultCredentials;                                
                        client.DownloadFile(new System.Uri(Siteurl + "/Shared%20Documents/Candidate%20Interview%20Guide%20Top.docx"), TempPath + "\\" + "Candidate Interview Guide Top.docx");
                    }

                    var doc = _app.Documents.Open(TempPath + "\\" + "Candidate Interview Guide Top.docx");
                   
                    InsertBookMark(doc, doc.Bookmarks["Candidate"], p.Candidate);
                    object objUnit = WdUnits.wdStory;
                    _app.Selection.EndKey(ref objUnit, _missing);
                    InsertPageBreak(doc);
                    var lastDocument = nameGroup.Last();
                    foreach (var stake in nameGroup)
                    {
                        if (stake.Equals(lastDocument))
                        {
                            ProcessCandidateQuestions(randomId, doc, stake, true);
                        }
                        else
                        {
                            ProcessCandidateQuestions(randomId, doc, stake, false);
                        }
                    }

                    if (!Directory.Exists(TempPath + "output"))
                    {
                        Directory.CreateDirectory(TempPath + "output");
                    }

                    const string pattern = "[\\~#%&*{}/:<>?|\"-,]";
                    const string replacement = "_";

                    var regEx = new Regex(pattern);
                    var jobnumber = Regex.Replace(regEx.Replace(p.JobNumber, replacement), @"\s+", " ");
                    var candidate = Regex.Replace(regEx.Replace(p.Candidate, replacement), @"\s+", " ");

                    var savepath = TempPath + jobnumber + "_" + candidate + "_" + nameGroup.Key + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".docx";
                    savepath = savepath.Replace('/', '-');
                    Console.WriteLine("savepath length: " + savepath.Length);
                    Logger.Info("savepath length: " + savepath.Length);
                    Console.WriteLine(savepath);
                    Logger.Info(savepath);
                    doc.SaveAs2(savepath);
                    doc.Close();

                    serverUrl = UploadCandidateDoc(savepath, p, idContext);
                            
                    Console.WriteLine("Work is complete for key: {0}", nameGroup.Key);
                    Logger.Info("Work is complete for key: {0}", nameGroup.Key);
                }
            }
            catch (Exception ex)
            {                
                object saveChanges2 = false;
                Console.WriteLine("Exception occurred :: ProcessCandidateDocuments :: " + ex.Message);
                Logger.Error("Exception occurred :: ProcessCandidateDocuments :: " + ex.Message);
                _app?.Quit(saveChanges2);
                _app = null;
                GC.Collect();             
            }
        }
        private static void ProcessQuestions(int randomId, Document doc, CompetencyQuestionsItem stake)
        {
            try
            {
                if (string.IsNullOrEmpty(stake.Path)) return;
                Console.WriteLine(stake.DocumentUrl);
                Logger.Info(stake.DocumentUrl);

                if (!Directory.Exists(TempPath))
                {
                    Directory.CreateDirectory(TempPath);
                }

                if (!Directory.Exists(TempPath + randomId.ToString()))
                {
                    Directory.CreateDirectory(TempPath + randomId.ToString());
                }

                using (var client = new WebClient())
                {
                    var split = stake.DocumentUrl.Split(',');
                    client.Credentials = CredentialCache.DefaultCredentials;
                    client.DownloadFile(new Uri(split[0]), TempPath + randomId.ToString() + "\\" + stake.Title + ".docx");

                    AddDocument(TempPath + randomId.ToString() + "\\" + stake.Title + ".docx", doc, false);
                    Directory.Delete(TempPath + randomId.ToString() + "\\", true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred :: ProcessQuestions :: " + ex.Message);
                Logger.Error("Exception occurred :: ProcessQuestions :: " + ex.Message);
            }
        }

        private static void ProcessCandidateQuestions(int randomId, Document doc, CandidateCompetencyQuestionsItem stake, bool lastDocument)
        {
            try
            {
                Console.WriteLine("In ProcessCandidateQuestions :: stake " + stake);
                Console.WriteLine("RandomId : " + randomId);
                Console.WriteLine("doc : " + doc.Path);
                if (string.IsNullOrEmpty(stake.Path)) return;
                Console.WriteLine(stake.DocumentUrl);
                Logger.Info(stake.DocumentUrl);

                if (!Directory.Exists(TempPath))
                {
                    Directory.CreateDirectory(TempPath);
                }

                if (!Directory.Exists(TempPath + randomId.ToString()))
                {
                    Directory.CreateDirectory(TempPath + randomId.ToString());
                }

                using (var client = new WebClient())
                {
                    var split = stake.DocumentUrl.Split(',');
                    client.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    client.DownloadFile(new System.Uri(split[0]), TempPath + randomId.ToString() + "\\" + stake.Title + ".docx");

                    if (!lastDocument)
                    {
                        AddCandidateDocument(TempPath + randomId.ToString() + "\\" + stake.Title + ".docx", doc, false);
                    }
                    else
                    {
                        AddCandidateDocument(TempPath + randomId.ToString() + "\\" + stake.Title + ".docx", doc, true);
                    }

                    Directory.Delete(TempPath + randomId.ToString() + "\\", true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred :: ProcessCandidateQuestions :: " + ex.Message);
                Logger.Error("Exception occurred :: ProcessCandidateQuestions :: " + ex.Message);
            }
        }

        private static string UploadDoc(string filePath, InterviewPanelItem ifi, IDC.InterviewGuideToolDataContext idc)
        {
            var rURL = "";
            var siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();

            var binary = System.IO.File.ReadAllBytes(filePath);
            var fname = System.IO.Path.GetFileName(filePath);
            var resourceUrl = siteurl + "/_api/web/GetFolderByServerRelativeUrl('" + "Interview%20Documents" + "')/Files/add(url='" + fname + "',overwrite=true)";
            Console.WriteLine(resourceUrl);
            Logger.Info(resourceUrl);
            var wreq = HttpWebRequest.Create(resourceUrl) as HttpWebRequest;
            //wreq.UseDefaultCredentials = false;
            //credential who has edit access on document library
            NetworkCredential credentials = (NetworkCredential)CredentialCache.DefaultCredentials; //new System.Net.NetworkCredential("username", "password", "domain");
                                                                                                              //  wreq.Credentials = credentials;
            wreq.Credentials = System.Net.CredentialCache.DefaultCredentials;

            //Get formdigest value from site
            var formDigest = GetFormDigestValue(siteurl, credentials);
            wreq.Headers.Add("X-RequestDigest", formDigest);
            wreq.Method = "POST";
            wreq.Timeout = 1000000; //timeout should be large in order to upload file which are of large size
            wreq.Accept = "application/json; odata=verbose";
            wreq.ContentLength = binary.Length;
            try
            {
                using (var requestStream = wreq.GetRequestStream())
                {
                    requestStream.Write(binary, 0, binary.Length);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred :: uploadDoc" + ex.Message);
                Logger.Error("Exception occurred :: uploadDoc" + ex.Message);
            }

            try
            {
                var wresp = wreq.GetResponse();
                using (var sr = new StreamReader(wresp.GetResponseStream()))
                {
                    var result = sr.ReadToEnd();
                    var jobj = JObject.Parse(result);

                    var child = jobj["d"]["ServerRelativeUrl"];
                    Console.WriteLine("Child " + child);
                    Console.WriteLine(child.ToString());
                    Logger.Info(child.ToString());
                    rURL = child.ToString();

                    var soMeta = jobj["d"]["Name"];

                    var iDocs = from x in idc.InterviewDocuments orderby x.Id descending select x;
                    var iDoc = (from z in iDocs where z.Name == soMeta.ToString() select z).First();
                    iDoc.HiringManager = ifi.HiringManager;
                    iDoc.HiringManagerId = ifi.HiringManagerId;
                    iDoc.JobNumber = ifi.JobNumber;
                    iDoc.JobTitle = ifi.JobTitle;
                    iDoc.TypeOfDocument = ifi.TypeOfDocument;                    

                    idc.UpdateObject(iDoc);
                    var newDocId = iDoc.Id;
                    idc.SaveChanges();                    

                    idc.UpdateObject(ifi);
                    idc.SaveChanges();
                }
            }
            catch (DataServiceRequestException ex2)
            {
                Console.WriteLine("DataServiceRequestException :: UploadDoc" + ex2.Message);
                Logger.Error("DataServiceRequestException :: UploadDoc" + ex2.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: UploadDoc" + ex.Message);
                Logger.Error("Exception :: UploadDoc" + ex.Message);
            }

            return rURL;
        }

        private static string UploadCandidateDoc(string filePath, CandidateInterviewItem ifi, IDC.InterviewGuideToolDataContext idc)
        {
            var rURL = "";
            var siteurl = ConfigurationManager.AppSettings["siteurl"].ToString();

            var binary = System.IO.File.ReadAllBytes(filePath);
            var fname = System.IO.Path.GetFileName(filePath);
            var result = string.Empty;
            var resourceUrl = siteurl + "/_api/web/GetFolderByServerRelativeUrl('" + "Interview%20Documents" + "')/Files/add(url='" + fname + "',overwrite=true)";
            Console.WriteLine(resourceUrl);
            Logger.Info(resourceUrl);
            var wreq = HttpWebRequest.Create(resourceUrl) as HttpWebRequest;
            var credentials = (NetworkCredential)System.Net.CredentialCache.DefaultCredentials; 
                                                                                                              
            wreq.Credentials = System.Net.CredentialCache.DefaultCredentials;

            //Get formdigest value from site
            var formDigest = GetFormDigestValue(siteurl, credentials);
            wreq.Headers.Add("X-RequestDigest", formDigest);
            wreq.Method = "POST";
            wreq.Timeout = 1000000; //timeout should be large in order to upload file which are of large size
            wreq.Accept = "application/json; odata=verbose";
            wreq.ContentLength = binary.Length;
            try
            {
                using (var requestStream = wreq.GetRequestStream())
                {
                    requestStream.Write(binary, 0, binary.Length);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception GetRequestStream :: UploadCandidateDoc" + ex.Message);
                Logger.Error("Exception GetRequestStream :: UploadCandidateDoc" + ex.Message);
            }

            try
            {
                var wresp = wreq.GetResponse();
                using (var sr = new System.IO.StreamReader(wresp.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    var jobj = JObject.Parse(result);

                    var child = jobj["d"]["ServerRelativeUrl"];
                    Console.WriteLine(child.ToString());
                    Logger.Info(child.ToString());
                    rURL = child.ToString();

                    var soMeta = jobj["d"]["Name"];
                    var iDocs = from x in idc.InterviewDocuments orderby x.Id descending select x;
                    var iDoc = (from z in iDocs where z.Name == soMeta.ToString() select z).First();
                    iDoc.HiringManager = ifi.HiringManager;
                    iDoc.HiringManagerId = ifi.HiringManagerId;
                    iDoc.JobNumber = ifi.JobNumber;
                    iDoc.JobTitle = ifi.JobTitle;
                    iDoc.TypeOfDocument = ifi.TypeOfDocument;
                    
                    idc.UpdateObject(iDoc);
                    idc.SaveChanges();

                    idc.UpdateObject(ifi);
                    idc.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: UploadCandidateDoc" + ex.Message);
                Logger.Error("Exception :: UploadCandidateDoc" + ex.Message);
            }

            return rURL;
        }

        //Method which return form digest value
        private static string GetFormDigestValue(string siteurl, NetworkCredential credentials)
        {
            var newFormDigest = "";
            var endpointRequest = (HttpWebRequest)HttpWebRequest.Create(siteurl + "/_api/contextinfo");
            endpointRequest.Method = "POST";
            endpointRequest.ContentLength = 0;
            endpointRequest.Credentials = credentials;
            endpointRequest.Accept = "application/json;odata=verbose";

            try
            {
                var endpointResponse = (HttpWebResponse)endpointRequest.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: GetFormDigestValue :: " + ex.Message);
                Logger.Error("Exception :: GetFormDigestValue :: " + ex.Message);
            }

            try
            {

                var webResp = endpointRequest.GetResponse();
                var webStream = webResp.GetResponseStream();
                var responseReader = new StreamReader(webStream);
                var response = responseReader.ReadToEnd();
                var j = JObject.Parse(response);
                var jObj = (JObject)JsonConvert.DeserializeObject(response);
                foreach (var item in jObj["d"].Children())
                {
                    Console.WriteLine("GetFormDigestValue :: item :: " + item);
                    newFormDigest = item.First()["FormDigestValue"].ToString();
                }
                responseReader.Close();

            }
            catch (Exception ex)
            {

                Console.WriteLine("Exception :: " + ex.Message);
                Logger.Error("Exception :: " + ex.Message);
            }

            return newFormDigest;
        }

        private static void AddDocument(string docname, Document doc, bool lastDocument)
        {
            var subDoc = _app.Documents.Open(docname);

            try
            {
                object docStart = doc.Content.End - 1;
                object docEnd = doc.Content.End;

                object start = subDoc.Content.Start;
                object end = subDoc.Content.End;

                var rng = doc.Range(ref docStart, ref docEnd);
                rng.FormattedText = subDoc.Range(ref start, ref end);

                if (!lastDocument)
                {
                    InsertPageBreak(doc);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: AddDocument :: " + ex.Message);
                Logger.Error("Exception :: AddDocument :: " + ex.Message);
            }
            finally
            {
                subDoc.Close(ref _missing, ref _missing, ref _missing);
            }
        }

        private static void AddCandidateDocument(string docname, Document doc, bool lastDocument)
        {
            var subDoc = _app.Documents.Open(docname);

            try
            {
                object docStart = doc.Content.End - 1;
                object docEnd = doc.Content.End;

                object start = subDoc.Content.Start;
                object end = subDoc.Content.End;

                var rng = doc.Range(ref docStart, ref docEnd);
                rng.FormattedText = subDoc.Range(ref start, ref end);

                if (!lastDocument)
                {
                    InsertPageBreak(doc);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: AddCandidateDocument :: " + ex.Message);
                Logger.Error("Exception :: AddCandidateDocument :: " + ex.Message);
            }
            finally
            {
                subDoc.Close(ref _missing, ref _missing, ref _missing);
            }
        }

        private static void InsertPageBreak(Document doc)
        {
            try
            {
                object docStart = doc.Content.End - 1;
                object docEnd = doc.Content.End;
                var rng = doc.Range(ref docStart, ref docEnd);

                object pageBreak = WdBreakType.wdPageBreak;
                rng.InsertBreak(ref pageBreak);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :: InsertPageBreak :: " + ex.Message);
                Logger.Error("Exception :: InsertPageBreak :: " + ex.Message);
            }
        }

        public static void InsertBookMark(Document doc, Bookmark bookmark, string newText)
        {
            try
            {
                object rng = bookmark.Range;
                var bookmarkName = bookmark.Name;
                bookmark.Range.Text = newText;

                doc.Bookmarks.Add(bookmarkName, ref rng);
            }
            catch(Exception e)
            {
                Logger.Error("Exception occurred :: InsertBookMark :: " + e.Message);
            }
        }
    }
}
